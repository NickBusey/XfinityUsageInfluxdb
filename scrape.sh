#!/bin/bash

echo "Scraping Xfinity.."
curl -G -XPOST http://$INFLUXDB_HOST:8086/query --data-urlencode "q=CREATE DATABASE homelabos"
curl -i -XPOST 'http://$INFLUXDB_HOST:8086/write?db=homelabos' --data-binary 'xfinity_usage value=$(xfinity-usage -j | jq .used)'
